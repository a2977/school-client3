export interface StudentInterface {
    id: string,
    name:  string,
    email: string,
    city:  string,
    course: string,
    school: string,
    imageId: string,
    cv: string
}
