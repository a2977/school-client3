
export interface CompanyInterface {
    id: string,
    name: string,
    email: string,
    profession: string,
    city: string,
    imageId: string,
}
