import {Divider} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React, {useState} from "react";
import "../App.css";
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import AppBar from "@material-ui/core/AppBar";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexDirection: "column",
        flexGrow: 1,
        overflow: "hidden",
        // padding: theme.spacing(0, 1),
        "& > *": {
            margin: theme.spacing(1),
        },
    },
    paper: {
        maxWidth: 1000,
        margin: `${theme.spacing(1)}px auto`,
        //padding: theme.spacing(2),
    },
    square: {
        width: 150,
        height: 150,
    },
    icon: {},

}));

export default function ImageAvatars() {
    const allStudents = ["jakob", "Eric Eriksson", "david"];
    const allEducations = ["java", "react", "sprinboot", "BärplockarAkademien"];
    const [name, setName] = useState("Niclas");
    const [education, setEducation] = useState(allEducations[3]);
    const [studentName, setStudentName] = useState(allStudents[1]);
    const [linkedinLink, setLinkedinLink] = useState("ww");
    const classes = useStyles();
    const history = useHistory();

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <HomeIcon className={classes.icon} onClick={() => history.push("/")}/>
                    <Typography variant="h5" color="inherit" align="center" style={{width: "100%"}}>
                        Info
                    </Typography>
                </Toolbar>
            </AppBar>
            <div className={classes.root}>
                <Grid container wrap="nowrap" style={{marginTop: 50}}>
                    <Grid item xs>
                        <Typography variant={"h6"} noWrap> Här har vi samlat viktiga dokument </Typography>
                    </Grid>
                </Grid>
                <Divider style={{margin: 40, backgroundColor: "#000000"}}/>
                <div style={{display: "block", margin: "auto", width: "30%"}}>
                    <form action="">
                        <label htmlFor="img">Ladda upp dokument: </label>
                        <input type="file" id="img" name="img" accept="image/*"/>
                        {/* <input type="submit"/> */}
                    </form>
                </div>
                <div style={{textAlign:"center", display:"flex",justifyContent:"center", width:"100%"}}>
                    <ul style={{marginRight: 200, marginTop: 100,}}>
                        <li>
                            <text>ashfasfhasf</text>
                        </li>
                        <li>
                            <text>ashfasfhasf</text>
                        </li>
                        <li>
                            <text>ashfasfhasf</text>
                        </li>
                        <li>
                            <text>ashfasfhasf</text>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}
