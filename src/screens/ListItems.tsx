import React, {FC, useState} from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import {StudentInterface} from "../interfaceCollection/StudentInterface";

interface studentInfo{
    name:string,
    email:string;
    city:string;
    course:string;
}

const ListItems = ({name,email,city,course}:studentInfo) => {
    return (
        <ListItem style={{flexDirection:"column",width:"300px"}} button>
            <ListItemText  primary={`${name}`}/>
            <ListItemText  primary={`${email}`}/>
            <ListItemText  primary={`${city}`}/>
            <ListItemText  primary={`${course}`}/>
            <hr style={{width:"100%"}}/>
        </ListItem>
    );
}
export default ListItems