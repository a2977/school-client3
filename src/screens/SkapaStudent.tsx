import React, {FC,useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import {useHistory} from "react-router-dom";
import {StudentInterface} from "../interfaceCollection/StudentInterface";


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    icon: {}
}));




const SignIn:FC = () => {
    const classes = useStyles();
    const history = useHistory();
    const [userInfo, setUserInfo] = useState<StudentInterface>({
        id:"",
        name:  "",
        email: "",
        city:  "",
        course: "",
        school: "",
        imageId: "",
        cv: ""
    });


    const handleChange = (event:React.ChangeEvent<HTMLInputElement|HTMLTextAreaElement>) => {
        setUserInfo({
            ...userInfo, [event.target.name]: event.target.value
        })

        console.log(userInfo);
    }

        function setStudent(): Promise<void | StudentInterface> {
        return fetch('http://localhost:8080/school/createStudentAccount', {
            method: "POST",
            headers: {'Content-Type': 'application/json;'},
            body: JSON.stringify(userInfo),
        })
            .then(res => res.json())
            .then((data)=>{console.log("yay",data)})

    }


    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <HomeIcon className={classes.icon} onClick={() => history.push("/")}/>
                    <Typography variant="h5" color="inherit" align="center" style={{width: "100%"}}>
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <AssignmentIndIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Skapa Student
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="name"
                            label="Namn"
                            id="name"
                            value={userInfo?.name}
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            value={userInfo?.email}
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="city"
                            label="Stad"
                            id="city"
                            value={userInfo?.city}
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="course"
                            label="Utbildning"
                            id="course"
                            value={userInfo?.course}
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="school"
                            label="Skola"
                            id="school"
                            value={userInfo?.school}
                            onChange={handleChange}
                        />
                        {/*<FormControlLabel*/}
                        {/*  control={<Checkbox value="remember" color="primary" />}*/}
                        {/*  label="Remember me"*/}
                        {/*/>*/}
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={setStudent}
                        >
                            Skapa
                        </Button>
                        {/*<Grid container>*/}
                        {/*  <Grid item xs>*/}
                        {/*    <Link href="#" variant="body2">*/}
                        {/*      Forgot password?*/}
                        {/*    </Link>*/}
                        {/*  </Grid>*/}
                        {/*</Grid>*/}
                    </form>
                </div>
            </Container>
        </div>
    );
}

export default SignIn;
