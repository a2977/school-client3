import React, {useEffect, useState} from "react";
import {StudentInterface} from "../interfaceCollection/StudentInterface";

function StudentList() {
    const [dataBundle, setDataBundle] = useState<StudentInterface[]>();

    useEffect(() => {
        fetchStudentData();
    }, [])

    const fetchStudentData = async () => {
        await fetch('http://localhost:8080/school/students')
            .then(item => item.json())
            .then(blob => {
                setDataBundle(blob)
                console.log(dataBundle)
            })
    }

    return (
        <div className="App">
            {dataBundle?.map(item => {
                return (
                    <div>
                        <p>{"namn: " + item.name}</p>
                        <p>{"email: " + item.email}</p>
                        <p>{"stad: " + item.city}</p>
                        <p>{"Utbildning: " + item.course}</p>
                        <p>{"Skola " + item.school}</p>
                        <br/>
                    </div>
                )
            })
            }
        </div>
    );
}


export default StudentList;
