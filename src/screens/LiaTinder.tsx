import React, {useEffect, useState} from "react";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "@material-ui/core/Divider";
import HomeIcon from "@material-ui/icons/Home";
import {AppBar, Toolbar} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {useHistory} from "react-router-dom";
import "../App.css";
import Button from "@material-ui/core/Button";
import {StudentInterface} from "../interfaceCollection/StudentInterface";
import ListItems from "./ListItems";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            margin: "auto",
        },
        rootList: {
            width: "100%",
           // alignItems: "center",
            display: "flex",
            flexDirection: "row",
            marginLeft: "20px",
            marginTop: "5%",
            flexWrap: "wrap"

        },
        rootList2: {
            backgroundColor: theme.palette.background.paper,
            flexDirection: "column",
        },
        cardHeader: {
            padding: theme.spacing(1, 2),
        },
        list: {
            width: 200,
            height: 230,
            backgroundColor: theme.palette.background.paper,
            overflow: "auto",
        },
        button: {
            margin: theme.spacing(0.5, 0),
        },
        icon: {},
    })
);

export default function TransferList() {
    const history = useHistory();
    const classes = useStyles();
    const [dataBundle, setDataBundle] = useState<StudentInterface[]>();
    const [markCities, setMarkCities] = useState([
        false,
        false,
        false,
        false,
        false,
    ]);
    const [markProfessions, setMarkprofessions] = useState([false, false]);
    const [flag, setFlag] = useState(false);

    let arr: StudentInterface[] = [];
    // @ts-ignore
    const [list, setList] = useState<StudentInterface[]>([arr]);
    useEffect(() => {
        fetchStudentData().catch((err) => console.log(err));
    }, []);

    const fetchStudentData = async () => {
        await fetch("http://localhost:8080/school/students")
            .then((item) => item.json())
            .then((blob) => {
                setDataBundle(blob);
            })
            .catch((err) => err.message)
            .catch((err) => console.log(err.message));
    };

    useEffect(() => {
        setList(arr);
    }, [markCities]);

    const MultiSelect = ({
                             title,
                             assortments,
                             onSelect,
                         }: {
        title: string;
        assortments: string[] | undefined;
        onSelect: (selected: string[]) => void;
    }) => {
        const [selected, setSelected] = useState<string[]>([]);
        const onCheckedClicked = (value: string) => {
            setSelected((prevState) => {
                const currentIndex = prevState.indexOf(value);
                if (currentIndex === -1) {
                    return [...prevState, value];
                } else {
                    prevState.splice(currentIndex, 1);
                    return prevState;
                }
            });
        };

        const handleCheck = (event: any, index: number) => {
            setList(arr);
            const saveCheck = [...markCities];
            saveCheck[index] = event.target.checked;
            setMarkCities(saveCheck);
            setFlag(true);
        };

        const handleFilter = (index: number, value: any) => {
            //@ts-ignore
            if (markCities[index]) {
                // @ts-ignore
                dataBundle
                    ?.filter((el) => el.city === value)
                    .map((el: any) => {
                        //@ts-ignore
                        if (!arr.includes(el)) {
                            arr.push(el);
                        }
                        console.log(el)
                    });
            }
        };

        return (
            <Card style={{}}>
                <CardHeader className={classes.cardHeader} title={title}/>
                <Divider/>
                <List className={classes.list} dense component="div" role="list">
                    {assortments
                        ?.filter((value, index, self) => self.indexOf(value) === index)
                        .map((value, index) => {
                            const labelId = `transfer-list-all-item-${value}-label`;
                            return (
                                <ListItem
                                    key={index}
                                    role="listitem"
                                    button
                                    onClick={() => {
                                        onCheckedClicked(value);
                                    }}
                                >
                                    <ListItemIcon>
                                        <Checkbox
                                            //@ts-ignore
                                            checked={markCities[index]}
                                            onChange={(event) => handleCheck(event, index)}
                                            //@ts-ignore
                                            onClick={handleFilter(index, value)}
                                            tabIndex={-1}
                                            disableRipple
                                            inputProps={{"aria-labelledby": labelId}}
                                        />
                                    </ListItemIcon>
                                    <ListItemText id={labelId} primary={` ${value}`}/>
                                </ListItem>
                            );
                        })}
                    <ListItem/>
                </List>
            </Card>
        );
    };

    return (
        <div>
            <AppBar position="sticky">
                <Toolbar>
                    <HomeIcon
                        className={classes.icon}
                        onClick={() => history.push("/")}
                    />
                    <Typography
                        variant="h5"
                        color="inherit"
                        align="center"
                        style={{width: "100%"}}
                    >
                        Lia Tinder
                    </Typography>
                </Toolbar>
            </AppBar>
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-start",
                    alignContent: "flex-start",
                }}
            >
                <div>
                    <Grid
                        container
                        direction="column"
                        justifyContent="center"
                        alignItems="flex-start"
                        className={classes.root}
                    >
                        <Grid item>
                            <Button
                                variant="text"
                                color="secondary"
                                onClick={() => {
                                    // @ts-ignore
                                    setMarkCities([false, false, false, false, false]);
                                    setFlag(false);
                                }}
                            >
                                all
                            </Button>
                            <MultiSelect
                                title="stad"
                                assortments={dataBundle
                                    ?.map((student) => student.city)
                                    .filter(onlyUnique)}
                                //@ts-ignore
                                onSelect={markCities}
                            />
                        </Grid>
                        {
                            <Grid item>
                                <MultiSelect
                                    title="kompetens"
                                    assortments={dataBundle
                                        ?.map((student) => student.course)
                                        .filter(onlyUnique)}
                                    //@ts-ignore
                                    onSelect={markProfessions}
                                />
                            </Grid>
                        }
                    </Grid>
                </div>
                <List dense className={classes.rootList}>
                    {flag && arr != null
                        ? list.map((value) => {
                            return (
                                <ListItems
                                    name={value.name}
                                    email={value.email}
                                    city={value.city}
                                    course={value.course}
                                />
                            );
                        })
                        : dataBundle?.map((value) => {
                            return (
                                <ListItems
                                    name={value.name}
                                    email={value.email}
                                    city={value.city}
                                    course={value.course}
                                />
                            );
                        })}
                </List>
            </div>
        </div>
    );
}

function onlyUnique(value: string, index: number, self: string[]) {
    return self.indexOf(value) === index;
}
