import React, {FC, useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import {useHistory} from "react-router-dom";
import {CompanyInterface} from "../interfaceCollection/CompanyInterface";


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    icon: {}
}));

const SignIn: FC = () => {
    const classes = useStyles();
    const history = useHistory();
    const [companyInfo, setCompanyInfo] = useState<CompanyInterface>({
        id: "",
        name: "",
        email: "",
        profession: "",
        city: "",
        imageId: ""
    });

    const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setCompanyInfo({
            ...companyInfo, [event.target.name]: event.target.value
        })

        console.log(companyInfo);
    }

    function setCompany(): Promise<void | CompanyInterface> {
        return fetch('http://localhost:8080/school/createCompanyAccount', {
            method: "POST",
            headers: {'Content-Type': 'application/json;'},
            body: JSON.stringify(companyInfo),
        })
            .then(res => res.json())
            .then((data) => {
                console.log("yay", data)
            })

    }

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <HomeIcon className={classes.icon} onClick={() => history.push("/")}/>
                    <Typography variant="h5" color="inherit" align="center" style={{width: "100%"}}>
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <MonetizationOnIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Skapa Arbetsgivare
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="name"
                            label="Företagsnamn"
                            id="name"
                            value={companyInfo?.name}
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            value={companyInfo?.email}
                            onChange={handleChange}
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="profession"
                            label="Profession"
                            id="profession"
                            value={companyInfo?.profession}
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="city"
                            label="Stad"
                            id="city"
                            value={companyInfo?.city}
                            onChange={handleChange}
                        />
                        {/*<FormControlLabel*/}
                        {/*  control={<Checkbox value="remember" color="primary" />}*/}
                        {/*  label="Remember me"*/}
                        {/*/>*/}
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={setCompany}
                        >
                            Skapa
                        </Button>
                        {/*<Grid container>*/}
                        {/*  <Grid item xs>*/}
                        {/*    <Link href="#" variant="body2">*/}
                        {/*      Forgot password?*/}
                        {/*    </Link>*/}
                        {/*  </Grid>*/}
                        {/*</Grid>*/}
                    </form>
                </div>
            </Container>
        </div>
    );
}
export default SignIn;
