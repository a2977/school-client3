import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import "./App.css";
import Skola from "./screens/Skola"
import SkapaStudent from "./screens/SkapaStudent"
import SkapaArbetsgivare from "./screens/SkapaArbetsgivare"
import Dokument from "./screens/Dokument";
import LiaTinder from "./screens/LiaTinder"

function App() {

  return (
      <div className="App">
        <Router>
          {/* <Header /> */}
          <Switch>
            <Route exact path="/">
              <Skola/>
            </Route>
            <Route exact path="/SkapaStudent">
              <SkapaStudent/>
            </Route>
            <Route exact path="/SkapaArbetsgivare">
              <SkapaArbetsgivare/>
            </Route>
            <Route exact path="/LiaTinder">
              <LiaTinder/>
            </Route>
            <Route exact path="/Dokument">
              <Dokument/>
            </Route>
          </Switch>
        </Router>
      </div>
  );
}


export default App;
