import React, {FC} from 'react';
import Button from '@material-ui/core/Button';
import {makeStyles, Theme, withStyles} from "@material-ui/core/styles";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TableCell from "@material-ui/core/TableCell";
import {Avatar, Divider, Grid} from "@material-ui/core";

type Student = {
    name: string,
    education: string,
    school: string,
    city: string,
    img: string
};

interface StudentProps {
    student: Student,
}

const StudentInfoDialog: FC<StudentProps> = ({student}): JSX.Element => {
    const [open, setOpen] = React.useState(false);
    const classes = useStyles();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <TableCell onClick={handleClickOpen} component="th" scope="row">
                {student.name}
            </TableCell>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <Grid container style={{margin: 10}}>
                    <Grid item direction="row">
                        <Avatar alt="" src={student.img} style={{height: "90%", width: 70}}/>
                    </Grid>
                    <Grid item direction="row">
                        <DialogTitle id="form-dialog-title">{student.name}</DialogTitle>
                    </Grid>
                </Grid>
                <DialogContent>
                    <DialogContentText>Utbildning: {student.education}</DialogContentText>
                    <DialogContentText>Skola: {student.school}, {student.city}</DialogContentText>
                </DialogContent>
                <Divider/>
                <Button onClick={handleClose} color="primary">
                    Spara i favoriter
                </Button>
                <Button onClick={handleClose} color="primary">
                    Besök profil
                </Button>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Stäng
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
const useStyles = makeStyles({})
export default StudentInfoDialog
